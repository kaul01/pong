
import turtle
import random

wn = turtle.Screen()
wn.title("Pong By kai")
wn.bgcolor("black")
wn.setup(width= 800 , height=600)
wn.tracer(0)


#score
score_a = 0
score_b = 0

#creating paddle a
paddle_a = turtle.Turtle()
paddle_a.speed(0)
paddle_a.shape("square")
paddle_a.shapesize(stretch_len=1 , stretch_wid=5)
paddle_a.color("blue")
paddle_a.penup()
paddle_a.goto(-350 , 0)

#creating paddle b
paddle_b = turtle.Turtle()
paddle_b.speed(0)
paddle_b.shape("square")
paddle_b.shapesize(stretch_len=1,stretch_wid=5)
paddle_b.color("red")
paddle_b.penup()
paddle_b.goto(350 , 0)


#creating ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape("circle")
ball.color("green")
ball.penup()
ball.goto(0 , 0)
ball.dx = 2
ball.dy = 2

#creting the pen
pen = turtle.Turtle()
pen.color("white")
pen.clear()
pen.penup()
pen.goto(0,260)
pen.write("Paddle A:{} Paddle B:{}".format(score_a , score_b) , align = "center" , font = ("cursive " , 24 , "bold"))
pen.penup()


def paddle_a_up():

    y = paddle_a.ycor()
    y += 40
    paddle_a.sety(y)

def paddle_a_down():
    y = paddle_a.ycor()
    y -= 40
    paddle_a.sety(y)

def paddle_b_up():
    y = paddle_b.ycor()
    y += 40
    paddle_b.sety(y)

def paddle_b_down():
    y = paddle_b.ycor()
    y -= 40
    paddle_b.sety(y)

def paddle_c_left():
    x = paddle_c.xcor()
    x -= 40
    paddle_c.setx(x)

def paddle_c_right():
    x = paddle_c.xcor()
    x += 40
    paddle_c.setx(x)






wn.listen()
wn.onkeypress(paddle_a_up, "a")
wn.onkeypress(paddle_a_down, "s")
wn.onkeypress(paddle_b_up, "Up")
wn.onkeypress(paddle_b_down, "Down")



while True:
    wn.update()

    ball.setx (ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)

    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy *= -1

    elif ball.ycor() < -290:
        ball.sety(-290)
        ball.dy *= -1

    if ball.xcor() < -430:
        ball.goto(0,0)
        ball.dx *= -1
        ball.dy *= -1
        score_b += 1
        pen.clear()
        pen.write("Paddle A:{}  Paddle B: {}".format(score_a , score_b)   , align="center", font=("cursive ", 24, "bold"))

    elif ball.xcor() > 430:
        ball.goto(0,0)
        ball.dx *= -1
        ball.dy *= -1
        score_a += 1
        pen.clear()
        pen.write("Paddle A:{}  Paddle B: {}".format(score_a , score_b)   , align="center", font=("cursive ", 24, "bold"))



    if ball.xcor() > 330  and (ball.ycor() < paddle_b.ycor() + 48 and ball.ycor() > paddle_b.ycor() - 48):
        ball.setx(330)
        ball.dx *= -1

    elif ball.xcor() < -330  and (ball.ycor() < paddle_a.ycor() + 48 and ball.ycor() > paddle_a.ycor() - 48):
        ball.setx(-330)
        ball.dx *= -1

    if paddle_a.ycor() > 450:
        paddle_a.sety(-450)

    elif paddle_a.ycor() < -450:
        paddle_a.sety(450)

    if paddle_b.ycor() > 400:
        paddle_b.sety(-400)

    elif paddle_b.ycor() < -400:
        paddle_b.sety(400)




